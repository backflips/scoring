<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Scoring Program</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
            
    
    </head>
    <body>
        <?php
            require_once("navbar.php");
            require_once("db_connect.php");
            $db = connect_to_db();
            
            $cat_query = "SELECT category_id, category_name FROM category";
            $club_query = "SELECT club_id, club_name FROM club";
            $discipline_query = "SELECT discipline_id, discipline_name FROM discipline";
            
            if(!($categories = $db->query($cat_query)) || !($clubs = $db->query($club_query)) || !($discipline = $db->query($discipline_query))) {
                ?>
        
        <div class="container">
            <div class="jumbotron">
                    <h2>ERROR!</h2>
                    <p>There was an error connecting to the databse.  Please try again later</p>
                    <p><a class="btn btn-primary" role="button" href="./">Back Home</a></p>
            </div>
        </div>
                <?php
            }
            else {
        ?>
        
        <div class="container">
            <form role="form" action="insertathlete.php" method="POST">
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-10">
                        <div class="col-xs-12 col-sm-2">
                            <label for="inputFirstName">Name</label>
                        </div>
                        <div class="col-xs-12 col-sm-5" id="firstname-group">
                            <input type="text" class="form-control" name="firstname" id="inputFirstName" placeholder="First Name">
                        </div>
                        <div class="col-xs-12 col-sm-5" id="lastname-group">
                            <input type="text" class="form-control" name="lastname" id="inputLastName" placeholder="Last Name">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-10" id="age-group">
                        <div class="col-xs-12 col-sm-2">
                            <label for="inputAge">Age</label>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <input type="number" class="form-control" name="age" id="inputAge" placeholder="Age">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-10" id="discipline-group">
                        <div class="col-xs-12 col-sm-2">
                            <label for="inputDiscipline">Discipline</label>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <select class="form-control" name="discipline" id="disciplineselect">
                                <option selected value="" style='display:none;'>Please Select a Discipline</option>
                                <?php while($disc_row = mysqli_fetch_array($discipline)) {?>
                                
                                <option value="<?php echo $disc_row['discipline_id'];?>"><?php echo $disc_row['discipline_name'];?></option>
                                <?php } ?>
                            
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-10" id="category-group">
                        <div class="col-xs-12 col-sm-2">
                            <label for="inputCategory">Category</label>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <select class="form-control" name="category" id="categoryselect">
                                <option selected value="" style='display:none;'>Please Select a Category</option>
                                <?php while($cat_row = mysqli_fetch_array($categories)) {?>
                                
                                <option value="<?php echo $cat_row['category_id'];?>"><?php echo $cat_row['category_name'];?></option>
                                <?php } ?>
                            
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-xs-12 col-sm-10" id="club-group">
                        <div class="col-xs-12 col-sm-2">
                            <label for="inputClub">Club</label>
                        </div>
                        <div class="col-xs-12 col-sm-5">
                            <select class="form-control" name="club" id="clubselect">
                                <option selected value="" style='display:none;'>Please Select a Club</option>
                                <?php while($club_row = mysqli_fetch_array($clubs)) {?>
                                
                                <option value="<?php echo $club_row['club_id'];?>"><?php echo $club_row['club_name'];?></option>
                                <?php } ?>
                            
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-10">
                        <div class="col-xs-12 col-sm-2">
                        </div>
                        <div class="col-xs-12 col-sm-10">
                            <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Athlete</button>
                            <a class="btn btn-danger" href="./"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        
        <?php
            }
        require_once("footer.php"); ?>

        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
        <script src="js/insertathlete.js"></script>
        
    </body>
</html>