<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Scoring Program</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">


    </head>
    <body>
        <?php
            require_once("navbar.php");
            require_once("db_connect.php");
            
            $db = connect_to_db();
            
            $disc_query = "SELECT discipline_id, discipline_name FROM discipline";
            
            if(!($discipline = $db->query($disc_query))) {
                ?>
                
        <div class="container">
            <div class="jumbotron">
                    <h2>ERROR!</h2>
                    <p>There was an error connecting to the databse.  Please try again later</p>
                    <p><a class="btn btn-primary" role="button" href="./">Back Home</a></p>
            </div>
        </div>
                <?php
            }
            else {
        ?>

        <div class="container">
            <form role="form" action="insertcategory.php" method="POST">
                <div class="form-group col-xs-12 col-sm-10" id="name-group">
                    <div class="col-xs-12 col-sm-2">
                        <label for="inputCatName">Category Name</label>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <input type="text" class="form-control" name="catname" id="inputCatName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-10" id="discipline-group">
                    <div class="col-xs-12 col-sm-2">
                        <label for="inputDiscipline">Discipline</label>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                            <select class="form-control" name="discipline" id="disciplineselect">
                                <option selected value="" style='display:none;'>Please Select a Discipline</option>
                                <?php while($disc_row = mysqli_fetch_array($discipline)) {?>
                                
                                <option value="<?php echo $disc_row['discipline_id'];?>"><?php echo $disc_row['discipline_name'];?></option>
                                <?php } ?>
                            
                            </select>
                        </div>
                </div>
                
                
                <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Category</button>
                        <a class="btn btn-danger" href="./"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                    </div>
                </div>
            </form>
        </div>


        <?php
            }
        
        require_once("footer.php"); ?>
    
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/insertcat.js"></script>
    </body>
</html>