<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Scoring Program</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
            
    
    </head>
    <body>
        <?php require_once("navbar.php"); ?>
        
        <div class="container">
            <form role="form" action="insertclub.php" method="POST">
                <div class="form-group col-xs-12 col-sm-10" id="name-group">
                    <div class="col-xs-12 col-sm-2">
                        <label for="inputClubName">Name</label>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <input type="text" class="form-control" name="clubname" id="inputClubName" placeholder="Name">
                    </div>
                </div>
                <div class="form-group col-xs-12 col-sm-10" id="location-group">
                    <div class="col-xs-12 col-sm-2">
                        <label for="inputClubLocation">Location</label>
                    </div>
                    <div class="col-xs-12 col-sm-5">
                        <input type="text" class="form-control" name="clublocation" id="inputClubLocation" placeholder="Location">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-2">
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Add Club</button>
                        <a class="btn btn-danger" href="./"><span class="glyphicon glyphicon-remove"></span> Cancel</a>
                    </div>
                </div>
            </form>
        </div>
        
        
        <?php require_once("footer.php"); ?>

        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/insertclub.js"></script>
    </body>
</html>