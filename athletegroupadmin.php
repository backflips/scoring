<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();
$errors = array();
$athlete_ids = array();


if(empty($_POST['athletes']) || empty($_POST['group_id']) || empty($_POST['action'])) {
    $errors['missing'] = "Missing information";
}

$athletes = $_POST['athletes'];
foreach($athletes as &$a){
    array_push($athlete_ids, validate($db, $a));
}
$group = validate($db, $_POST['group_id']);
$action = validate($db, $_POST['action']);

if($action == 'remove'){    
    foreach($athlete_ids as $athlete){    
        $query = "CALL remove_athlete_group('$athlete','$group')";        
        if($result = $db->query($query)){
            $data['success'] = true;
        }
        else {
            $errors['remove'] = "Could not remove athlete";
            break;
        }
    }
}
else if($action == 'add') {
    foreach($athlete_ids as $athlete){    
        $query = "CALL add_athlete_group('$athlete','$group')";
        if($result = $db->query($query)){
            $data['success'] = true;
        }
        else {
            $errors['add'] = $db->error;
            break;
        }
    }
}
else if($action == 'moveup') {
    foreach($athlete_ids as $athlete){    
        $query = "CALL move_athlete_order_up('$athlete','$group')";
        
        if($result = $db->query($query)){
            $data['success'] = true;
        }
        else {
            $errors['moveup'] = "Could not move athlete";
            break;
        }
    }
}
else if($action == 'movedown') {
    foreach($athlete_ids as $athlete){    
        $query = "CALL move_athlete_order_down('$athlete','$group')";
        
        if($result = $db->query($query)){
            $data['success'] = true;
        }
        else {
            $errors['movedown'] = "Could not move athlete";
            break;
        }
    }
}

if(!empty($errors)){
    $data['success'] = false;
    $data['errors'] = $errors;
}

echo json_encode($data);
?>