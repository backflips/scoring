<?php
    function connect_to_db() {
        error_reporting(E_ERROR);
        $config = parse_ini_file("config.ini", 1);
        if($config != True) {
            echo "<div class='container col-xs-12 col-md-10' id='message'><div class='alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center'>Could not open config file</div></div> ";
        }
        $db = new mysqli($config['database']['db_host'], $config['database']['db_username'], $config['database']['db_password'], $config['database']['db_name']);
        
        if(mysqli_connect_errno()) {
            echo "<div class='container col-xs-12 col-md-10' id='message'><div class='alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center'>Database Error: " . mysqli_connect_error() . "</div></div> ";
        }
        return $db;
    }
    
    function validate($db, $data){
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    $data = mysqli_real_escape_string($db, $data);
    return $data;
    }
    
    function newlog($message) {
        $file = 'debug.log';
        $now = new DateTime();
        $outmessage = $now->format('Y-m-d H:i:s') . " : " . $message . "\n";
        
        file_put_contents($file, $outmessage, FILE_APPEND);
    }
?>
