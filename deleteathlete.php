<?php
require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['athlete_id']) || !ctype_digit($_POST['athlete_id'])) {
        $errors['athlete_id'] = 'Please provide an athlete id number';
    }
    if(empty($_POST['athlete_name'])){
        $errors['athlete_name'] = 'Please provide an ahtlete name';
    }
    
    if(!empty($errors)) {
            $data['success'] = false;
            $data['errors'] = $errors;
    }
    else {
        $athlete_name = validate($db, $_POST['athlete_name']);
        $athlete_id = validate($db, $_POST['athlete_id']);
        
        $deletequery = "DELETE FROM athlete WHERE athlete_id=" . $athlete_id;
        if($result = $db->query($deletequery)){
            $data['success'] = true;
            $data['message'] = 'Deleted ' . $athlete_name . '!';
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to delete.  There was an error with the database! ' . $db->error;
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);

?>