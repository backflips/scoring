<?php
require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['category_id']) || !ctype_digit($_POST['category_id'])) {
        $errors['category_id'] = 'Please provide an category id number';
    }
    if(empty($_POST['category_name'])){
        $errors['category_name'] = 'Please provide an category name';
    }
    
    if(!empty($errors)) {
            $data['success'] = false;
            $data['errors'] = $errors;
    }
    else {
        $category_name = validate($db, $_POST['category_name']);
        $category_id = validate($db, $_POST['category_id']);
        
        $deletequery = "DELETE FROM category WHERE category_id=" . $category_id;
        if($result = $db->query($deletequery)){
            $data['success'] = true;
            $data['message'] = 'Deleted ' . $category_name . '!';
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to delete.  Database error: ' . $db->error;
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);

?>