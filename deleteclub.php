<?php
require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['club_id']) || !ctype_digit($_POST['club_id'])) {
        $errors['club_id'] = 'Please provide an athlete id number';
    }
    if(empty($_POST['club_name'])){
        $errors['club_name'] = 'Please provide an club name';
    }
    
    if(!empty($errors)) {
            $data['success'] = false;
            $data['errors'] = $errors;
    }
    else {
        $club_name = validate($db, $_POST['club_name']);
        $club_id = validate($db, $_POST['club_id']);
        
        $deletequery = "DELETE FROM club WHERE club_id=" . $club_id;
        if($result = $db->query($deletequery)){
            $data['success'] = true;
            $data['message'] = 'Deleted ' . $club_name . '!';
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to delete.  There was an error with the database!';
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);

?>