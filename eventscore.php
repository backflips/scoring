<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Enter Score</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
        <link href="css/scoringmodal.css" rel="stylesheet">
    
    </head>
    <body>
        
        <?php require_once("navbar.php"); ?>
        
        <div class="container">
            <div class="col-xs-12 col-sm-12" id="selectRow">
                <div class="col-xs-12 col-sm-2">
                    <select class="form-control" id="sessionSelect" name="session">
                            <option select value="" style='display:none;'>Session</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2 col-sm-offset-1">
                    <select class="form-control" id="disciplineSelect" name="discipline">
                            <option select value="" style='display:none;'>Discipline</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2 col-sm-offset-1">
                    <select class="form-control" id="eventSelect" name="event">
                            <option select value="" style='display:none;'>Event</option>
                    </select>
                </div>
                <div class="col-xs-12 col-sm-2 col-sm-offset-1">
                    <select class="form-control" id="rotationSelect" name="rotation">
                            <option select value="" style='display:none;'>Rotation</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div id="athlete-table" class="col-xs-10 col-xs-offset-1">
                <table class="table table-bordered table-hover table-condensed">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Club</th>
                            <th>Category</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                
            </div>
        </div>
        <div id="scoreModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="scoreModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h2 class="modal-title" id="scoreModalLabel">Enter Score</h4>
                        <h3 class="modal-title" id="athlete_info"></h5>
                    </div>
                    <div class="modal-body">
                        <form class="form-inline" role="form" id="scoreForm">
                            <input id="athleteID" type="hidden">
                            <input id="scoreID" type="hidden">
                            <h3>D-Score</h5>
                            <div class="row dscore-group">
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12">
                                    <label for="inputD1Score">D1-Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputD1Score">
                                </div>
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12 pull-right">
                                    <label for="inputDScore">D-Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputDScore" disabled>
                                </div>
                            </div>
                            <h3>E-Score</h5>
                            <div class="row escore-group">
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12">
                                    <label for="inputE1Score">E1-Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputE1Score">
                                </div>
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12">
                                    <label for="inputE2Score">E2-Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputE2Score">
                                </div>
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12 pull-right">
                                    <label for="inputEScore">E-Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputEScore" disabled>
                                </div>
                            </div>
                            <h3>Neutral Deductions</h5>
                            <div class="row">
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12">
                                    <label for="inputNDScore">Neutral Deductions</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputNDScore">
                                </div>
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12 pull-right">
                                    <label for="inputNDFScore">Neutral Deductions</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputNDFScore" disabled>
                                </div>
                            </div>
                            <h3>Final Score</h5>
                            <div class="row">
                                <div class="form-group col-lg-2 col-md-12 col-xs-12 col-sm-12 pull-right finalscore-group">
                                    <label for="inputFinalScore">Final Score</label>
                                    <input type="number" class="form-control pull-right input-sm" id="inputFinalScore" disabled>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="save-button" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php require_once("footer.php"); ?>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/eventscore.js"></script>
    </body>
</html>