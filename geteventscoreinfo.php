<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    //Change Session
    if(!empty($_POST['session']) && (empty($_POST['discipline']) && empty($_POST['event']) && empty($_POST['rotation']))) {
        $session = (int)validate($db, $_POST['session']);
        $discipline_query = "SELECT d.discipline_id, d.discipline_name
                                FROM discipline as d JOIN discipline_session as ds ON (d.discipline_id = ds.discipline_id)
                                WHERE ds.session_id=$session";
                                
        if($discipline_result = $db->query($discipline_query)) {
            $discipline_array = array();
            while($disc_row = mysqli_fetch_array($discipline_result)) {
                array_push($discipline_array, $disc_row);
            }
            $data['disciplines'] = $discipline_array;
        }
    }
    //Change Discipline
    if(!empty($_POST['discipline']) && empty($_POST['event']) && empty($_POST['rotation']) && !empty($_POST['session'])) {
        $discipline = (int)validate($db, $_POST['discipline']);
        $session = (int)validate($db, $_POST['session']);
        
        
        $event_query =      "SELECT e.event_id, e.event_name
                            FROM event as e JOIN event_discipline as ed ON (e.event_id = ed.event_id) JOIN event_order as eo ON (e.event_id = eo.event_id)
                            WHERE ed.discipline_id=$discipline
                            ORDER BY eo.event_order ASC";
                        
        $event_num_query =  "SELECT count(e.event_id) as e_total
                            FROM event as e 
                            JOIN event_discipline as ed ON (e.event_id=ed.event_id)
                            JOIN discipline_session as ds ON (ds.discipline_id = ed.discipline_id)
                            WHERE ds.discipline_id = '$discipline' AND ds.session_id = '$session'";
                            
        $group_num_query =  "SELECT count(gs.group_id) as g_total
                            FROM group_session as gs
                            JOIN group_discipline as gd ON (gs.group_id = gd.group_id)
                            WHERE gs.session_id = '$session' AND gd.discipline_id = '$discipline'";
        
        //Get event list               
        if($event_result = $db->query($event_query)) {
            $event_array = array();
            while($event_row = mysqli_fetch_array($event_result)) {
                array_push($event_array, $event_row);
            }
            $data['events'] = $event_array;
        }
        //Get number of Rotations
        if(($event_result = $db->query($event_num_query)) && ($group_result = $db->query($group_num_query))) {    
            $event_row = mysqli_fetch_array($event_result);
            $group_row = mysqli_fetch_array($group_result);
            
            $event_num = $event_row['e_total'];
            $group_num = $group_row['g_total'];
            
            $rotations = max($event_num,$group_num);
            
            $data['rotations'] = $rotations;
        }
    }
    //Get athlete table data
    if(!empty($_POST['session']) && !empty($_POST['discipline']) && !empty($_POST['event']) && !empty($_POST['rotation'])) {
        $session = (int)validate($db, $_POST['session']);
        $discipline = (int)validate($db, $_POST['discipline']);
        $event = (int)validate($db, $_POST['event']);
        $rotation = (int)validate($db, $_POST['rotation']);
        
        $event_order_query = "SELECT event_order
                              FROM event_order
                              WHERE event_id = '$event'";
                              
        if($event_order_result = $db->query($event_order_query)){
            $event_order_row = mysqli_fetch_array($event_order_result);
            $event_order = $event_order_row['event_order'];
            $group = $event_order - $rotation + 1;
            if($group <= 0){
                $group += 6;
            }
            $athlete_query = "SELECT a.athlete_id, a.first_name, a.last_name, c.club_name, cat.category_name, s.final_score
                              FROM athlete AS a INNER JOIN athlete_club AS ac ON (ac.athlete_id=a.athlete_id)
                                                INNER JOIN club AS c ON (c.club_id = ac.club_id)
                                                INNER JOIN athlete_category AS acat ON (acat.athlete_id=a.athlete_id)
                                                INNER JOIN category AS cat ON (cat.category_id=acat.category_id)
                                                INNER JOIN athlete_group AS ag ON (ag.athlete_id=a.athlete_id)
                                                INNER JOIN groups AS g ON (g.group_id = ag.group_id)
                                                INNER JOIN group_discipline AS gd ON (gd.group_id = ag.group_id)
                                                INNER JOIN group_session as gs ON (gs.group_id = ag.group_id)
                                                LEFT JOIN athlete_score as a_score ON (a.athlete_id = a_score.athlete_id AND a_score.event_id = '$event' AND gs.session_id = a_score.session_id)
                                                LEFT JOIN score as s ON (a_score.score_id=s.score_id)
                              WHERE g.group_number = '$group' AND gd.discipline_id = '$discipline' AND gs.session_id = '$session'";
                              
            if($athlete_result = $db->query($athlete_query)){
                $return_array = array();
                while($athlete_row = mysqli_fetch_array($athlete_result)){
                    $a_info = array();
                    $a_info['athlete_id'] = $athlete_row['athlete_id'];
                    $a_info['athlete_name'] = $athlete_row['first_name'] . ' ' . $athlete_row['last_name'];
                    $a_info['club_name'] = $athlete_row['club_name'];
                    $a_info['category_name'] = $athlete_row['category_name'];
                    if(empty($athlete_row['final_score'])) {
                        $a_info['score'] = "";
                    } else{
                        $a_info['score'] = $athlete_row['final_score'];
                    }
                    array_push($return_array, $a_info);
                }
                $data['athletes'] = $return_array;
            }
        }
    }
}
else {
    $session_query = "SELECT s.session_id, s.session_number
                          FROM sessions as s";
    if($session_result = $db->query($session_query)){
        $session_array = array();
        while($session_row = mysqli_fetch_array($session_result)) {
            array_push($session_array, $session_row);
        }
        $data['session'] = $session_array;
    }
}

echo json_encode($data);
?>