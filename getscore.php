<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();
$errors = array();

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(empty($_POST['athlete_id'])) {
        $errors['athlete'] = "Error retrieveing athlete id.";
    }
    if(empty($_POST['event_id'])) {
        $errors['athlete'] = "Error retrieveing athlete id.";
    }
    if(empty($_POST['session_id'])) {
        $errors['athlete'] = "Error retrieveing athlete id.";
    }
    
    if( ! empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {        
        $athlete_id = validate($db, $_POST['athlete_id']);
        $event_id = validate($db, $_POST['event_id']);
        $session_id = validate($db, $_POST['session_id']);
        
        $score_query =      "SELECT *
                            FROM score as s
                                JOIN athlete_score AS a ON (a.score_id = s.score_id)
                            WHERE a.athlete_id = '$athlete_id' && a.event_id = '$event_id' && a.session_id = '$session_id'";
                            
        if($score_result = $db->query($score_query)) {
            $score_row = mysqli_fetch_array($score_result);
            $data['success'] = true;
            $data['score_row'] = $score_row;
        }
        else{
            $data['success'] = false;
            $errors['mysql'] = 'Unable to add.  There was an error with the database!' . $db->error;
            $data['errors'] = $errors;
        }
        
    }
    
}


echo json_encode($data);
?>