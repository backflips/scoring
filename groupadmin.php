<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>View Athletes</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
        <link href="css/groups.css" rel="stylesheet">
    
    </head>
    <body>
         <?php require_once("navbar.php");            
            require_once('db_connect.php');
        ?>
        
        <div class="container">
            <div class="col-xs-6 col-xs-offset-3">
                <h2 class="text-center">Group Setup</h2>
            <label>Session</label>
            <select class="form-control session-select">
                <option select value='' style='display:none;'>Session</option>
            </select>
            </div>
        </div>
        
        <div class="container">
            <div class="row filter-row">
                <div class="col-md-5">
                    <h4>Filters</h4>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Club</label>
                        </div>
                        <div class="col-md-9 col-md-offset-1">
                            <select class="form-control club-select">
                                <option value=''>Club</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="custom-select">Category</label>
                        </div>
                        <div class="col-md-9 col-md-offset-1">
                            <select class="form-control custom-select category-select">
                                <option value=''>Category</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-2">
                    <h4>Group</h4>
                    <div class="row">
                        <div class="col-md-2">
                            <label>Discipline</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control discipline-select">
                                <option select value='' style='display:none;'>Discipline</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <label class="custom-select">Group Number</label>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control custom-select group-select">
                                <option select value='' style='display:none;'>Group Number</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row select-row">
                <div class="col-md-5">
                    <div class = "col-xs-12">
                        <label>Athletes</label>
                        <select multiple class="form-control athletes" id="athletes">
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                        <div class="buttons">
                            <div class="col-md-12 col-sm-6 col-xs-6">
                                <button class="btn btn-success btn-custom" id="btn-add"><span class="glyphicon glyphicon-arrow-right"></span></button>
                            </div>
                            <div class="col-md-12 col-sm-6 col-xs-6">
                                <button class="btn btn-danger btn-custom" id="btn-remove"><span class="glyphicon glyphicon-arrow-left"></span></button>
                            </div>
                        </div>
                </div>
                <div class="col-md-5 col-xs-12">
                    <div class = "col-xs-10">
                        <label>Group Order</label>
                        <select multiple class="form-control group" id="group">
                        </select>
                    </div>
                    <div class="col-xs-1">
                        <div class="buttons">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-custom2" id="btn-grp-up"><span class="glyphicon glyphicon-chevron-up"></span></button>
                            </div>
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-custom2" id="btn-grp-down"><span class="glyphicon glyphicon-chevron-down"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        
        <?php require_once("footer.php"); ?>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
        <script src="js/groupadmin.js"></script>
        
    </body>
</html>