<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();
$errors = array();


if(!empty($_POST['session_id'])) {
    if(!empty($_POST['discipline_id'])){
        if(!empty($_POST['group_id'])){
            $group = validate($db, $_POST['group_id']);
            $session = validate($db, $_POST['session_id']);
            $discipline = validate($db, $_POST['discipline_id']);
            
            $club_query = "SELECT DISTINCT(c.club_id), c.club_name
                           FROM club AS c
                           JOIN athlete_club as ac ON (ac.club_id = c.club_id)
                           JOIN athlete_session as asession ON (ac.athlete_id=asession.athlete_id)
                           JOIN athlete_discipline as ad ON (ac.athlete_id=ad.athlete_id)
                           WHERE asession.session_id = '$session' AND ad.discipline_id = '$discipline'";
                           
            $category_query = "SELECT DISTINCT(c.category_id), c.category_name
                           FROM category AS c
                           JOIN athlete_category as ac ON (ac.category_id = c.category_id)
                           JOIN athlete_session as asession ON (ac.athlete_id=asession.athlete_id)
                           JOIN athlete_discipline as ad ON (ac.athlete_id=ad.athlete_id)
                           WHERE asession.session_id = '$session' AND ad.discipline_id = '$discipline'";
            
            $group_query = "SELECT a.athlete_id, a.first_name, a.last_name, cat.category_name, c.club_name
                            FROM athlete as a
                                JOIN athlete_category as acat ON (acat.athlete_id = a.athlete_id)
                                JOIN category as cat ON (cat.category_id = acat.category_id)
                                JOIN athlete_club as aclub ON (aclub.athlete_id = a.athlete_id)
                                JOIN club  as c ON (c.club_id = aclub.club_id)
                                JOIN athlete_group as ag ON (a.athlete_id = ag.athlete_id)
                                JOIN athlete_group_order as ago ON (a.athlete_id = ago.athlete_id)
                            WHERE ag.group_id = '$group' ORDER BY ago.order_number";
                            
            $athlete_query = "SELECT a.athlete_id, a.first_name, a.last_name, cat.category_name, c.club_name
                            FROM athlete as a
                                JOIN athlete_category as acat ON (acat.athlete_id = a.athlete_id)
                                JOIN category as cat ON (cat.category_id = acat.category_id)
                                JOIN athlete_club as aclub ON (aclub.athlete_id = a.athlete_id)
                                JOIN club  as c ON (c.club_id = aclub.club_id)
                                LEFT JOIN athlete_group as ag ON (a.athlete_id = ag.athlete_id)
                                JOIN athlete_session as asession ON (a.athlete_id = asession.athlete_id)
                                JOIN athlete_discipline as ad ON (a.athlete_id = ad.athlete_id)
                            WHERE ag.group_id IS NULL AND asession.session_id = '$session' AND ad.discipline_id = '$discipline'";
                            
            if(($group_result = $db->query($group_query)) && ($athletes_result = $db->query($athlete_query)) && ($club_result = $db->query($club_query)) && ($category_result = $db->query($category_query))) {
                $group_array = array();
                $athlete_array = array();
                $club_array = array();
                $category_array = array();
                while($group_row = mysqli_fetch_array($group_result)) {
                    array_push($group_array, $group_row);
                }
                while($athlete_row = mysqli_fetch_array($athletes_result)){
                    array_push($athlete_array, $athlete_row);
                }
                while($club_row = mysqli_fetch_array($club_result)) {
                    array_push($club_array, $club_row);
                }
                while($category_row = mysqli_fetch_array($category_result)){
                    array_push($category_array, $category_row);
                }
                $data['group'] = $group_array;
                $data['athletes'] = $athlete_array;
                $data['club'] = $club_array;
                $data['category'] = $category_array;
            }
        }
        else {
            $session = validate($db, $_POST['session_id']);
            $discipline = validate($db, $_POST['discipline_id']);
            $groupnum_query = "SELECT g.group_id, g.group_number
                               FROM groups AS g
                                    JOIN group_discipline AS gd ON ( g.group_id = gd.group_id ) 
                                    JOIN group_session AS gs ON ( g.group_id = gs.group_id ) 
                               WHERE gs.session_id = '$session' AND gd.discipline_id = '$discipline'";
                                 
            if($groupnum_result = $db->query($groupnum_query)) {
                $groupnum_array = array();
                while($groupnum_row = mysqli_fetch_array($groupnum_result)) {
                    array_push($groupnum_array, $groupnum_row);
                }
                $data['groups'] = $groupnum_array;
            }
        }
    }
    else {
        $session = validate($db, $_POST['session_id']);
        $discipline_query =     "SELECT d.discipline_id, d.discipline_name
                                 FROM discipline as d
                                        JOIN discipline_session as ds ON (d.discipline_id = ds.discipline_id)
                                 WHERE ds.session_id = '$session'";
                                 
        if($discipline_result = $db->query($discipline_query)) {
            $discipline_array = array();
            while($discipline_row = mysqli_fetch_array($discipline_result)) {
                array_push($discipline_array, $discipline_row);
            }
            $data['discipline'] = $discipline_array;
        }
    }
}
else {
    $session_query =    "SELECT session_id, session_number
                         FROM sessions";
                         
    if($session_result = $db->query($session_query)) {
        $session_array = array();
        while($session_row = mysqli_fetch_array($session_result)) {
            array_push($session_array, $session_row);
        }
        $data['sessions'] = $session_array;
    }
}



echo json_encode($data);