<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>Scoring Program</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
    
    </head>
    <body>
        <?php require_once("navbar.php");?>    
        <div class="container">
        
            <!-- Main component for a primary marketing message or call to action -->
            <div class="jumbotron">
                <h1>Welcome to the Site</h1>
                <p>Place Holder</p>
                <p>Another Place Holder</p>
                <p>
                <button class="btn btn-lg btn-primary" data-toggle="modal" data-target="#testModal">This does nothing</button>
                </p>
            </div>
        
        </div> <!-- /container -->
        
        <!-- Modal -->
        <div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="testModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="testModalLabel">TESTING</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-muted">This is a test</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        
        <?php require_once("footer.php"); ?>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>