<?php

require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['firstname'])) {
        $errors['firstname'] = 'First name is required.';
    }
    if(empty($_POST['lastname'])) {
        $errors['lastname'] = 'Last name is required.';
    }
    if(empty($_POST['age'])) {
        $errors['age'] = 'Age is required';
    }
    if(!ctype_digit($_POST['age'])) {
        $errors['age'] = 'Age must be a number';
    }
    if(empty($_POST['discipline'])) {
        $errors['discipline'] = 'Discipline is required.';
    }
    if(empty($_POST['category'])) {
        $errors['category'] = 'Category is required.';
    }
    if(empty($_POST['club'])) {
        $errors['club'] = 'Club is required.';
    }
    
    if( ! empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $firstname = validate($db, $_POST['firstname']);
        $lastname = validate($db, $_POST['lastname']);
        $age = (int)validate($db, $_POST['age']);
        $discipline = validate($db, $_POST['discipline']);
        $category = (int)validate($db, $_POST['category']);
        $club = (int)validate($db, $_POST['club']);
        
        $insert_query = "CALL insert_athlete('$firstname','$lastname','$age','$club','$category','$discipline')";
        
        //$insertquery = "INSERT INTO athlete(first_name, last_name, age, gender, category_id, club_id)
        //                VALUES('$firstname', '$lastname', '$age', '$gender', '$category', '$club')";
        
        if($result = $db->query($insert_query)) {
            $data['success'] = true;
            $data['message'] = 'Added ' . $firstname . ' ' . $lastname;
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to add.  There was an error with the database!' . $db->error;
            $errors['stuff'] = $result;
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);