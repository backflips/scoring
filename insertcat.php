<?php

require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['category_name'])) {
        $errors['category_name'] = 'Category name is required.';
    }
    if(empty($_POST['discipline'])) {
        $errors['discipline'] = 'Discipline is required.';
    }
    
    if( ! empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $category_name = validate($db, $_POST['category_name']);
        $discipline = validate($db, $_POST['discipline']);
        
        $insert_query = "CALL insert_cat('$category_name','$discipline')";
        
        //$insertquery = "INSERT INTO athlete(first_name, last_name, age, gender, category_id, club_id)
        //                VALUES('$firstname', '$lastname', '$age', '$gender', '$category', '$club')";
        
        if($result = $db->query($insert_query)) {
            $data['success'] = true;
            $data['message'] = 'Added ' . $category_name;
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to add.  There was an error with the database!' . $db->error;
            $errors['stuff'] = $result;
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);