<?php

require_once('db_connect.php');
$db = connect_to_db();

$errors = array();
$data = array();

    if(empty($_POST['clubname'])) {
        $errors['clubname'] = 'Name is required.';
    }
    if(empty($_POST['clublocation'])) {
        $errors['clublocation'] = 'Location is required.';
    }
    
    if( ! empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $clubname = validate($db, $_POST['clubname']);
        $location = validate($db, $_POST['clublocation']);
        
        $insert_query = "INSERT INTO club(club_name, club_location)
                                    VALUES('$clubname','$location')";
        
        if($result = $db->query($insert_query)) {
            $data['success'] = true;
            $data['message'] = 'Added ' . $clubname . '!';
        }
        else {
            $data['success'] = false;
            $errors['mysql'] = 'Unable to add club.  There was an error with the database!';
            $data['errors'] = $errors;
        }
    }
    
    echo json_encode($data);