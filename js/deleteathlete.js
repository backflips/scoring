$(document).ready(function() {
    $('.delete-link').click(function(event) {
        
        $('#message').remove();
        
        var id = $(this).attr('id');        
        var athlete_name = $('#athlete'+id).find('td').html();
            
        var formData = {
            'athlete_id'    : parseInt(id),
            'athlete_name'  : athlete_name
        };
            
        $.ajax({
            type        :   'POST',
            url         :   'deleteathlete.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
            
            .done(function(data) {                
                if (! data.success) {
                    if (data.errors.athlete_id) {
                        $('#athlete-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.athlete_id + '!</div></div>')
                    }
                    if (data.errors.athlete_name) {
                        $('#athlete-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.athlete_name + '!</div></div>')
                    }
                    if (data.errors.mysql) {
                        $('#athlete-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>')
                    }
                }
                else{
                    $('#athlete-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                    $('#athlete'+id).remove();
                }
                console.log(data);
            })
            .fail(function(data) {
                console.log(data);
            })
        event.preventDefault();
    });
});