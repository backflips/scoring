$(document).ready(function() {
    $('.delete-link').click(function(event) {
        
        $('#message').remove();
        
        var id = $(this).attr('id');        
        var category = $('#category'+id).find('td').html();
            
        var formData = {
            'category_id'    : parseInt(id),
            'category_name'  : category
        };
            
        $.ajax({
            type        :   'POST',
            url         :   'deletecat.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
            
            .done(function(data) {                
                if (! data.success) {
                    if (data.errors.category_id) {
                        $('#category-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.category_id + '!</div></div>')
                    }
                    if (data.errors.category_name) {
                        $('#category-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.category_name + '!</div></div>')
                    }
                    if (data.errors.mysql) {
                        $('#category-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>')
                    }
                }
                else{
                    $('#category-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                    $('#category'+id).remove();
                }
                console.log(data);
            })
            .fail(function(data) {
                console.log(data);
            })
        event.preventDefault();
    });
});