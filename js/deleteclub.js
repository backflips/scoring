$(document).ready(function() {
    $('.delete-link').click(function(event) {
        
        $('#message').remove();
        
        var id = $(this).attr('id');        
        var club = $('#club'+id).find('td').html();
            
        var formData = {
            'club_id'    : parseInt(id),
            'club_name'  : club
        };
            
        $.ajax({
            type        :   'POST',
            url         :   'deleteclub.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
            
            .done(function(data) {                
                if (! data.success) {
                    if (data.errors.club_id) {
                        $('#club-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.club_id + '!</div></div>')
                    }
                    if (data.errors.club_name) {
                        $('#club-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.club_name + '!</div></div>')
                    }
                    if (data.errors.mysql) {
                        $('#club-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>')
                    }
                }
                else{
                    $('#club-table').before('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                    $('#club'+id).remove();
                }
                console.log(data);
            })
            .fail(function(data) {
                console.log(data);
            })
        event.preventDefault();
    });
});