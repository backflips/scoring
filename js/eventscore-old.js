$(document).ready(function() {
    
    var index;   
    var table_shown = false;
    
    setInterval(function(){
        if (table_shown) {
            $('#athlete-table> table > tbody > tr').each(function() {
                //alert($(this).attr('id'));                
            });
        }
    }, 10000);
    
    console.log($('#sessionSelect').val());
    
    $.ajax({
            type        :   'GET',
            url         :   'geteventscoreinfo.php',
            data        :   "",
            dataType    :   'json',
            encode      :   true            
        })
        .done(function(data) {
                if(data.session){
                    $('#sessionSelect option').remove();
                    $('#sessionSelect').append("<option select value='' style='display:none;'>Session</option>");
                    $('#disciplineSelect option').remove();
                    $('#disciplineSelect').append("<option select value='' style='display:none;'>Discipline</option>");
                    $('#eventSelect option').remove();
                    $('#eventSelect').append("<option select value='' style='display:none;'>Event</option>");
                    $('#rotationSelect option').remove();
                    $('#rotationSelect').append("<option select value='' style='display:none;'>Rotation</option>");
                    table_shown = false;
                    console.log($('#disciplineSelect').val());
                    for (index = 0; index < data.session.length; ++index) {
                        $('#sessionSelect').append('<option value='+data.session[index].session_id+'>'+data.session[index].session_number+'</option>');
                    }
                    
                    $('#sessionSelect').val("");
                }
            });
    
    $('#sessionSelect').change(function() {
        
        $('#athlete-table td').remove();
        
        var formData = {
            'session': $('#sessionSelect').val(),
        };
        
        $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
        })
        
            .done(function(data) {
                //console.log(data);
                if(data.discipline){
                    $('#disciplineSelect option').remove();
                    $('#disciplineSelect').append("<option select value='' style='display:none;'>Discipline</option>");
                    $('#eventSelect option').remove();
                    $('#eventSelect').append("<option select value='' style='display:none;'>Event</option>");
                    $('#rotationSelect option').remove();
                    $('#rotationSelect').append("<option select value='' style='display:none;'>Rotation</option>");
                    table_shown = false;
                    for (index = 0; index < data.discipline.length; ++index) {
                        $('#disciplineSelect').append('<option value='+data.discipline[index].discipline_id+'>'+data.discipline[index].discipline_name+'</option>');
                    }
                }
            })
        
    });
    
    $('#disciplineSelect').change(function() {
       
       $('#athlete-table td').remove();
       
       var formData = {
            'discipline': $('#disciplineSelect').val(),
       };

       $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
        })
       
            .done(function(data) {
                //console.log(data);
                if(data.event){
                    $('#eventSelect option').remove();
                    $('#eventSelect').append("<option select value='' style='display:none;'>Event</option>");
                    $('#rotationSelect option').remove();
                    $('#rotationSelect').append("<option select value='' style='display:none;'>Rotation</option>");
                    table_shown = false;
                    for (index = 0; index < data.event.length; ++index) {
                        $('#eventSelect').append('<option value='+data.event[index].event_id+'>'+data.event[index].event_name+'</option>');
                    }
                }
            })
       
    });
    
    $('#eventSelect').change(function() {
       
       $('#athlete-table td').remove();
       
       var formData = {
            'session'   : $('#sessionSelect').val(),
            'discipline': $('#disciplineSelect').val(),
       };
       
       $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
        })
       
            .done(function(data) {
                //console.log(data);
                if(data.rotations){
                    $('#rotationSelect option').remove();
                    $('#rotationSelect').append("<option select value='' style='display:none;'>Rotation</option>");
                    table_shown= false;
                    for (index = 0; index < data.rotations; ++index) {
                        $('#rotationSelect').append('<option value='+(index+1)+'>'+(index+1)+'</option>');
                    }
                }
            })
       
    });
    
    $('#rotationSelect').change(function() {
        
        $('#athlete-table tbody tr').remove();
        
       var formData = {
            'session'   : $('#sessionSelect').val(),
            'discipline': $('#disciplineSelect').val(),
            'event': $('#eventSelect').val(),
            'rotation': $('#rotationSelect').val(),
       };
       
       $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
        })
       
            .done(function(data) {
                $('#athlete-table').show();
                if(data.athletes){
                    for(index = 0; index < data.athletes.length; index++) {
                        //console.log(data.athletes[index].athlete_id);
                        $('#athlete-table tbody:last').append('<tr id="athlete'+data.athletes[index].athlete_id+'"><td>'+data.athletes[index].athlete_id +'</td><td>'+data.athletes[index].athlete_name+'</td><td>'+data.athletes[index].club_name+'</td><td>'+data.athletes[index].category_name+'</td><td>'+data.athletes[index].score+'</td></tr>');
                    }
                    table_shown = true;
                    $('.table-hover tbody tr').click(function() {
                        var athlete_num = $(this).find('td:first-child').text();
                        var athlete_name = $(this).find('td:nth-child(2)').text();
                        $('#scoreModal').find('#athlete_info').html(athlete_num + ' ' + athlete_name);
                        id = $(this).attr('id');
                        alert(id.replace(/[^0-9]/g, ""));
                        //alert($(this).find('td:first-child').text());
                        //$('#scoreModal').modal('show');
                    })
                }
            })       
    });

    
    /*$('form').submit(function(event) {
        
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $('#message').remove();
        
        var formData = {
            'category_name': $('input[name=catname]').val(),
            'discipline': $('#disciplineselect').val(),
        };
        
        $.ajax({
            type        :   'POST',
            url         :   'insertcat.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
        
            .done(function(data) {
                
                console.log(data);
                
                if ( ! data.success) {
                    if (data.errors.category_name) {
                        $('#firstname-group').addClass('has-error');
                        $('#firstname-group').append('<div class="help-block">' + data.errors.club_name + '</div');
                    }
                    if (data.errors.discipline) {
                        $('#lastname-group').addClass('has-error');
                        $('#lastname-group').append('<div class="help-block">' + data.errors.discipline + '</div');
                    }
                    if (data.errors.mysql) {
                        $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                    }
                } else {
                    $('form').trigger("reset");
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                }
            })
            .fail(function(data) {
                console.log(data);               
            });
        event.preventDefault();
    });*/
});