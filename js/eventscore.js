var removeTableData = function() {
    $('#athlete-table > table >tbody > tr').remove();
}

var resetDiscipline = function() {
    $('#disciplineSelect option').remove();
    $('#disciplineSelect').append("<option select value='' style='display:none;'>Discipline</option>");
}
var resetEvents = function() {
    $('#eventSelect option').remove();
    $('#eventSelect').append("<option select value='' style='display:none;'>Event</option>");
}
var resetRotations = function() {
    $('#rotationSelect option').remove();
    $('#rotationSelect').append("<option select value='' style='display:none;'>Rotation</option>");
}
var setDScore = function() {
    var D1 = parseFloat($('#inputD1Score').val());
    $('#inputDScore').val(D1);    
}
var setNDScore = function() {
    var ND = parseFloat($('#inputNDScore').val());
    $('#inputNDFScore').val(ND);    
}
var setEscore = function() {
    var E1 = parseFloat($('#inputE1Score').val());
    var E2 = parseFloat($('#inputE2Score').val());
    if (!E1) {
        E1 = E2;
    }
    else if (!E2) {
        E2 = E1;
    }
    var E = 10.0 - ((E1+E2)/2);
    E = E.toFixed(2);
    $('#inputEScore').val(E);
}
var setFinalScore = function() {
    var D = parseFloat($('#inputDScore').val());
    var E = parseFloat($('#inputEScore').val());
    var ND = parseFloat($('#inputNDFScore').val());
    
    if (!ND) {
        ND = 0.0;
    }
    
    var final = D+E+ND;
    final = final.toFixed(2);
    $('#inputFinalScore').val(final);
}
var setModalScore = function() {
    $('#message').remove();
    
    var athlete_id = $('#athleteID').val();
    var event_id = $('#eventSelect').val();
    var session_id = $('#sessionSelect').val();
    var formData = {
        'athlete_id'    :   athlete_id,
        'event_id'      :   event_id,
        'session_id'    :   session_id
    };
    
    $('#athleteID').val(athlete_id);
    
    $.ajax({
        type        :   'POST',
        url         :   'getscore.php',
        data        :   formData,
        dataType    :   'json',
        encode      :   true 
    })
        .done(function(data) {
                console.log(data);
                if (!data.success) {
                    if (data.errors.mysql) {
                        $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                    }
                }
                else{
                    if (data.score_row) {
                        if(data.score_row.d_score){
                            $('#inputD1Score').val(data.score_row.d_score);
                        }
                        if (data.score_row.e1_score) {
                            $('#inputE1Score').val(data.score_row.e1_score);
                        }
                        if (data.score_row.e2_score) {
                            $('#inputE2Score').val(data.score_row.e2_score);
                        }
                        if (data.score_row.nd_score) {
                            $('#inputNDScore').val(data.score_row.nd_score);
                        }
                        $('#scoreID').val(data.score_row.score_id);
                    }
                    setDScore();
                    setEscore()
                    setNDScore();
                    setFinalScore();
                }
                
        })
    
}
var setModal = function() {
    var athlete_num = $(this).find('td:first-child').text();
    var athlete_name = $(this).find('td:nth-child(2)').text();
    var athlete_id = $(this).attr('id');
    athlete_id = athlete_id.replace(/[^0-9]/g, "");
    $('#scoreModal').find('#athlete_info').html(athlete_num + ' ' + athlete_name);
    $('#athleteID').val(athlete_id);
    $('#scoreForm').trigger("reset");
    $('#scoreID').val('');
    setModalScore();
    $('#scoreModal').modal('show');   
}
var saveScore = function() {
    var formData = {
        'athlete_id'    :   $('#athleteID').val(),
        'event_id'      :   $('#eventSelect').val(),
        'session_id'    :   $('#sessionSelect').val(),
        'd_score'       :   $('#inputDScore').val(),
        'e_score'       :   $('#inputEScore').val(),
        'e1_score'      :   $('#inputE1Score').val(),
        'e2_score'      :   $('#inputE2Score').val(),
        'nd_score'      :   $('#inputNDFScore').val(),
        'final_score'   :   $('#inputFinalScore').val(),
        'score_id'      :   $('#scoreID').val()
    };
    
    $.ajax({
            type        :   'POST',
            url         :   'savescore.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data) {
            console.log(data);
            if (!data.success) {
                if (data.errors.d_score) {
                    $('.dscore-group').addClass('has-error');
                    $('.dscore-group').append('<div class="help-block">' + data.errors.d_score + '</div');
                }
                if (data.errors.e_score) {
                    $('.escore-group').addClass('has-error');
                    $('.escore-group').append('<div class="help-block">' + data.errors.e_score + '</div');
                }
                if (data.errors.final_score) {
                    $('.finalscore-group').addClass('has-error');
                    $('.finalscore-group').append('<div class="help-block">' + data.errors.e_score + '</div');
                }
                if (data.errors.mysql) {
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                }
                if (data.errors.athlete) {
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.athlete + '!</div></div>');
                }
                if (data.errors.event) {
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.event + '!</div></div>');
                }
                if (data.errors.session) {
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.session + '!</div></div>');
                }
            }
            else{
                $('#scoreModal').modal('hide');
                refreshScores();
            }
        })
    
}

var getData = function() {
       
    var formData = {
        'session'   :   $('#sessionSelect').val(),
        'discipline':   $('#disciplineSelect').val(),
        'event'     :   $('#eventSelect').val(),
        'rotation'  :   $('#rotationSelect').val()
    };
    $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data){
            console.log(data);
            if (data.disciplines) {
                for (index = 0; index < data.disciplines.length; ++index) {
                    $('#disciplineSelect').append('<option value='+data.disciplines[index].discipline_id+'>'+data.disciplines[index].discipline_name+'</option>');
                }
            }
            if (data.events) {
                for (index = 0; index < data.events.length; ++index) {
                    $('#eventSelect').append('<option value='+data.events[index].event_id+'>'+data.events[index].event_name+'</option>');
                }
            }
            if (data.rotations) {
                for (index = 0; index < data.rotations; ++index) {
                    $('#rotationSelect').append('<option value='+(index+1)+'>'+(index+1)+'</option>');
                }
            }
            if (data.athletes) {
                $('#athlete-table').show();
                removeTableData();
                for(index = 0; index < data.athletes.length; index++) {
                    $('#athlete-table tbody:last').append('<tr id="athlete'+data.athletes[index].athlete_id+'"><td>'+data.athletes[index].athlete_id +'</td><td>'+data.athletes[index].athlete_name+'</td><td>'+data.athletes[index].club_name+'</td><td>'+data.athletes[index].category_name+'</td><td>'+data.athletes[index].score+'</td></tr>');
                }
                $('.table > tbody > tr').click(setModal);
            }
            else {
                $('#athlete-table').hide();
            }
        });
}

var refreshScores = function() {
    var formData = {
        'session'   :   $('#sessionSelect').val(),
        'discipline':   $('#disciplineSelect').val(),
        'event'     :   $('#eventSelect').val(),
        'rotation'  :   $('#rotationSelect').val()
    };
    $.ajax({
            type        :   'POST',
            url         :   'refreshscore.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true,
            timeout     :   3000
    })
    .done(function(data) {
        console.log(data);
        if (data.scores) {
            for (index = 0; index < data.scores.length; ++index) {
                $('#athlete'+data.scores[index].athlete_id).find("td").eq(4).html(data.scores[index].score);
            }
        }
    })
    .fail(function(jqXHR, textStatus){
        alert("The database is taking too long to respond.  Please check your connection");  
    });
}

$(document).ready(function() {
    
    $('#athlete-table').hide();
    
    $.ajax({
            type        :   'GET',
            url         :   'geteventscoreinfo.php',
            data        :   "",
            dataType    :   'json',
            encode      :   true        
    })
    .done(function(data) {
        if (data.session){
            for (index = 0; index < data.session.length; ++index) {
                $('#sessionSelect').append('<option value='+data.session[index].session_id+'>'+data.session[index].session_number+'</option>');
            }
        }
    });
    
    setInterval(function(){
            refreshScores();
    }, 10000);
    
    $('#sessionSelect').change(function() {
        resetDiscipline();
        resetEvents();
        resetRotations();
        getData();
    });
    
    $('#disciplineSelect').change(function() {
        resetEvents();
        resetRotations();
        getData();
    });
    
    $('#eventSelect').change(function() {
        getData();
    });
    
    $('#rotationSelect').change(function() {
        getData();
    });
    
    $('#inputD1Score').change(function(){
        setDScore();
        setFinalScore();
    });
    
    $('#inputE1Score').change(function(){
        setEscore();
        setFinalScore();
    });
    $('#inputE2Score').change(function(){
        setEscore();
        setFinalScore();
    });
    
    $('#inputNDScore').change(function(){
        setNDScore();
        setFinalScore();
    });
    
    $('#save-button').click(saveScore);
    
});