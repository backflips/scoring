function resetDiscipline() {
    $('.discipline-select option').remove();
    $('.discipline-select').append("<option select value='' style='display:none;'>Discipline</option>");
}
function resetGroupNumber() {
    $('.group-select option').remove();
    $('.group-select').append("<option select value='' style='display:none;'>Group Number</option>");
}
function resetGroup() {
    $('.group option').remove();
    $('.athletes option').remove();
}
function resetClub() {
    $('.club-select option').remove();
    $('.club-select').append("<option value=''>Club</option>");
}
function resetCategory() {
    $('.category-select option').remove();
    $('.category-select').append("<option value=''>Category</option>");
}

function modify_Athlete(athlete_id, group_id, action) {
    var formData = {
        'athlete_id'    :   athlete_id,
        'group_id'      :   group_id,
        'action'        :   action
    };
    
    $.ajax({
            type        :   'POST',
            url         :   'athletegroupadmin.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data) { 
            console.log(formData);
            if (data.success) {
            }
        })
    
};

function modify_Athletes(athletes, group_id, action) {
    var formData = {
        'athletes'      :   athletes,
        'group_id'      :   group_id,
        'action'        :   action
    };
    
    $.ajax({
            type        :   'POST',
            url         :   'athletegroupadmin.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data) { 
            console.log(data);
            if (data.success) {
            }
        })
    
};

function sortAthletes() {
    var options = $('#athletes option');
    var arr = options.map(function(_,o) {
        return {t: $(o).text(), v: o.value};
    }).get();
    arr.sort(function(o1,o2) {
        return o1.t >o2.t ? 1 : o1.t < o2.t ? -1 : 0;
    });
    options.each(function(i, o) {
        o.value = arr[i].v;
        $(o).text(arr[i].t);
    }); 
};

function getGroupData(selectType) {
    var formData = {
        'session_id'    :   $('.session-select').val(),
        'discipline_id' :   $('.discipline-select').val(),
        'group_id'      :   $('.group-select').val(),
        'club_id'       :   $('.club-select').val(),
        'category_id'   :   $('.category_select').val()
    };
    $.ajax({
            type        :   'POST',
            url         :   'groupadmindata.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data) { 
            console.log(data);
            if (selectType == 1) {
                if (data.sessions) {
                    for (index = 0; index < data.sessions.length; ++index) {
                        $('.session-select').append('<option value='+data.sessions[index].session_id+'>'+data.sessions[index].session_number+'</option>');
                    }
                }
            }
            else if (selectType == 2) {
                if (data.discipline) {
                    for (index = 0; index < data.discipline.length; ++index) {
                        $('.discipline-select').append('<option value='+data.discipline[index].discipline_id+'>'+data.discipline[index].discipline_name+'</option>');
                    }
                }
            }
            else if (selectType == 3) {
                if (data.groups) {
                    for (index = 0; index < data.groups.length; ++index) {
                        $('.group-select').append('<option value='+data.groups[index].group_id+'>'+data.groups[index].group_number+'</option>');
                    }
                }
            }
            else if (selectType == 4) {
                if (data.group) {
                    for (index = 0; index < data.group.length; ++index) {
                        $('.group').append('<option value='+data.group[index].athlete_id+'>'+data.group[index].first_name+' '+data.group[index].last_name+' - '+data.group[index].club_name +' ('+data.group[index].category_name+')</option>');
                    }
                }
                if (data.athletes) {
                    for (index = 0; index < data.athletes.length; ++index) {
                        $('.athletes').append('<option value='+data.athletes[index].athlete_id+'>'+data.athletes[index].first_name+' '+data.athletes[index].last_name +' - '+data.athletes[index].club_name +' ('+data.athletes[index].category_name+')</option>');
                    }
                }
                if (data.club) {
                    for (index = 0; index < data.club.length; ++index) {
                        $('.club-select').append('<option value='+data.club[index].club_id+'>'+data.club[index].club_name+'</option>');
                    }
                }
                if (data.category) {
                    for (index = 0; index < data.category.length; ++index) {
                        $('.category-select').append('<option value='+data.category[index].category_id+'>'+data.category[index].category_name+'</option>');
                    }
                }
            }
        })
};
$(document).ready(function() {
    if($(document).width() < 970){
        $('.glyphicon-arrow-right').addClass('glyphicon-arrow-down');
        $('.glyphicon-arrow-right').addClass('glyphicon-arrow-right');
        $('.glyphicon-arrow-left').addClass('glyphicon-arrow-up');
        $('.glyphicon-arrow-left').addClass('glyphicon-arrow-left');                    
    }
    getGroupData(1);
    sortAthletes();
});
            
$('#btn-add').click(function() {
    var athletes = [];
    $('#athletes option:selected').each(function(n) {
        athletes[n] = $(this).val();
        $('#group').append("<option value="+ $(this).val() +">"+$(this).text()+"</option>");
        $(this).remove();
    });
    modify_Athletes(athletes, $('.group-select').val(), 'add');
});
$('#btn-remove').click(function() {
    var athletes = [];
    $('#group option:selected').each(function(n) {
        athletes[n] = $(this).val();
        $('#athletes').append("<option value="+ $(this).val() +">"+$(this).text()+"</option>");
        $(this).remove();
    });
    modify_Athletes(athletes, $('.group-select').val(), 'remove');
    sortAthletes();
});
$('#btn-grp-up').click(function(){
    var athletes = [];
    $('#group option:selected').each(function(n) {
        athletes[n] = $(this).val();
        $(this).insertBefore($(this).prev());
    });
    modify_Athletes(athletes, $('.group-select').val(), 'moveup');
});
$('#btn-grp-down').click(function(){
    var athletes = [];
    $($('#group option:selected').get().reverse()).each(function(n) {
        athletes[n] = $(this).val();
        $(this).insertAfter($(this).next()); 
    });
    modify_Athletes(athletes, $('.group-select').val(), 'movedown');
});
$('.session-select').change(function() {
    resetDiscipline();
    resetGroupNumber();
    resetGroup();
    resetCategory();
    resetClub();
    getGroupData(2);
});
$('.discipline-select').change(function() {
    resetGroupNumber();
    resetGroup();
    resetCategory();
    resetClub();
    getGroupData(3);
});
$('.group-select').change(function() {
    resetGroup();
    resetCategory();
    resetClub();
    getGroupData(4);
});