$(document).ready(function() {
    
    $('form').submit(function(event) {
        
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $('#message').remove();
        
        var formData = {
            'firstname': $('input[name=firstname]').val(),
            'lastname': $('input[name=lastname]').val(),
            'age': $('input[name=age]').val(),
            'discipline': $('#disciplineselect').val(),
            'category': $('#categoryselect').val(),
            'club': $('#clubselect').val()
        };
        
        $.ajax({
            type        :   'POST',
            url         :   'insertathlete.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
        
            .done(function(data) {
                
                console.log(data);
                
                if ( ! data.success) {
                    if (data.errors.firstname) {
                        $('#firstname-group').addClass('has-error');
                        $('#firstname-group').append('<div class="help-block">' + data.errors.firstname + '</div');
                    }
                    if (data.errors.lastname) {
                        $('#lastname-group').addClass('has-error');
                        $('#lastname-group').append('<div class="help-block">' + data.errors.lastname + '</div');
                    }
                    if (data.errors.age) {
                        $('#age-group').addClass('has-error');
                        $('#age-group').append('<div class="help-block">' + data.errors.age + '</div');
                    }
                    if (data.errors.discipline) {
                        $('#discipline-group').addClass('has-error');
                        $('#discipline-group').append('<div class="help-block">' + data.errors.discipline + '</div');
                    }
                    if (data.errors.category) {
                        $('#category-group').addClass('has-error');
                        $('#category-group').append('<div class="help-block">' + data.errors.category + '</div');
                    }
                    if (data.errors.club) {
                        $('#club-group').addClass('has-error');
                        $('#club-group').append('<div class="help-block">' + data.errors.club + '</div');
                    }
                    if (data.errors.mysql) {
                        $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                    }
                } else {
                    $('form').trigger("reset");
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                }
            })
            .fail(function(data) {
                console.log(data);               
            });
        event.preventDefault();
    });
});