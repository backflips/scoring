$(document).ready(function() {
    
    $('form').submit(function(event) {
        
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $('#message').remove();
        
        var formData = {
            'category_name': $('input[name=catname]').val(),
            'discipline': $('#disciplineselect').val(),
        };
        
        $.ajax({
            type        :   'POST',
            url         :   'insertcat.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
        
            .done(function(data) {
                
                console.log(data);
                
                if ( ! data.success) {
                    if (data.errors.category_name) {
                        $('#firstname-group').addClass('has-error');
                        $('#firstname-group').append('<div class="help-block">' + data.errors.club_name + '</div');
                    }
                    if (data.errors.discipline) {
                        $('#lastname-group').addClass('has-error');
                        $('#lastname-group').append('<div class="help-block">' + data.errors.discipline + '</div');
                    }
                    if (data.errors.mysql) {
                        $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                    }
                } else {
                    $('form').trigger("reset");
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                }
            })
            .fail(function(data) {
                console.log(data);               
            });
        event.preventDefault();
    });
});