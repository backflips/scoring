$(document).ready(function() {
    
    $('form').submit(function(event) {
        
        $('.form-group').removeClass('has-error');
        $('.help-block').remove();
        $('#message').remove();
        
        var formData = {
            'clubname': $('input[name=clubname]').val(),
            'clublocation': $('input[name=clublocation]').val()
        };
        
        $.ajax({
            type        :   'POST',
            url         :   'insertclub.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true
        })
        
            .done(function(data) {
                
                console.log(data);
                
                if ( ! data.success) {
                    if (data.errors.clubname) {
                        $('#name-group').addClass('has-error');
                        $('#name-group').append('<div class="help-block">' + data.errors.clubname + '</div');
                    }
                    if (data.errors.clublocation) {
                        $('#location-group').addClass('has-error');
                        $('#location-group').append('<div class="help-block">' + data.errors.clublocation + '</div');
                    }
                    if (data.errors.mysql) {
                        $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-danger col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.errors.mysql + '!</div></div>');
                    }
                } else {
                    $('form').trigger("reset");
                    $('form').prepend('<div class="container col-xs-12 col-md-10" id="message"><div class="alert alert-success col-md-10 col-md-offset-2 col-xs-12 text-center">' + data.message + '</div></div>');
                }
            })
            .fail(function(data) {
                console.log(data);               
            });
        event.preventDefault();
    });
});