var removeTableData = function() {
    $('#athlete-table td').remove();
}

var getData = function() {
    var formData = {
        'session'   :   $('#sessionSelect').val(),
        'discipline':   $('#disciplineSelect').val(),
        'event'     :   $('#eventSelect').val(),
        'rotation'  :   $('#rotationSelect').val()
    };
    $.ajax({
            type        :   'POST',
            url         :   'geteventscoreinfo.php',
            data        :   formData,
            dataType    :   'json',
            encode      :   true            
    })
        .done(function(data){
            console.log(data);
            if (data.disciplines) {
                for (index = 0; index < data.discipline.length; ++index) {
                    $('#disciplineSelect').append('<option value='+data.discipline[index].discipline_id+'>'+data.discipline[index].discipline_name+'</option>');
                }
            }
            if (data.events) {
                for (index = 0; index < data.event.length; ++index) {
                    $('#eventSelect').append('<option value='+data.event[index].event_id+'>'+data.event[index].event_name+'</option>');
                }
            }
            if (data.rotations) {
                for (index = 0; index < data.rotations; ++index) {
                    $('#rotationSelect').append('<option value='+(index+1)+'>'+(index+1)+'</option>');
                }
            }
            if (data.athletes) {
                for(index = 0; index < data.athletes.length; index++) {
                    $('#athlete-table tbody:last').append('<tr id="athlete'+data.athletes[index].athlete_id+'"><td>'+data.athletes[index].athlete_id +'</td><td>'+data.athletes[index].athlete_name+'</td><td>'+data.athletes[index].club_name+'</td><td>'+data.athletes[index].category_name+'</td><td>'+data.athletes[index].score+'</td></tr>');
                }
            }
        });
}

$(document).ready(function() {
    
    $.ajax({
            type        :   'GET',
            url         :   'geteventscoreinfo.php',
            data        :   "",
            dataType    :   'json',
            encode      :   true            
    })
    .done(function(data) {
        if (data.session){
            for (index = 0; index < data.session.length; ++index) {
                $('#sessionSelect').append('<option value='+data.session[index].session_id+'>'+data.session[index].session_number+'</option>');
            }
        }
    });
    
    $('#sessionSelect').change(function() {
        getData();
    });
    
    $('#disciplineSelect').change(function() {
        getData();
    });
    
    $('#eventSelect').change(function() {
        getData();
    });
    
    $('#rotationSelect').change(function() {
        getData();
    });
    
});