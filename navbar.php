<!-- Fixed navbar -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="./">Scoring Program</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="./">Home</a></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Athletes <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="addathlete.php">Add an Athlete</a></li>
                                <li><a href="viewathletes.php">View Atheltes</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Clubs <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="addclub.php">Add a Club</a></li>
                                <li><a href="viewclubs.php">View Clubs</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span></a>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="addcategory.php">Add a Category</a></li>
                                <li><a href="viewcategories.php">View Categories</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="./eventscore.php">eventscore</a>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div> <!-- /container -->        