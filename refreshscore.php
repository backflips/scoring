<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    
    if(!empty($_POST['session']) && !empty($_POST['discipline']) && !empty($_POST['event']) && !empty($_POST['rotation'])) {
        $session = (int)validate($db, $_POST['session']);
        $discipline = (int)validate($db, $_POST['discipline']);
        $event = (int)validate($db, $_POST['event']);
        $rotation = (int)validate($db, $_POST['rotation']);
        
        $event_order_query = "SELECT event_order
                              FROM event_order
                              WHERE event_id = '$event'";
                              
        if($event_order_result = $db->query($event_order_query)){
            $event_order_row = mysqli_fetch_array($event_order_result);
            $event_order = $event_order_row['event_order'];
            $group = $event_order - $rotation + 1;
            if($group <= 0){
                $group += 6;
            }
            
            $score_query = "SELECT a.athlete_id, s.final_score
                            FROM score as s
                                JOIN athlete_score 		as a 	ON (s.score_id = a.score_id)
                                JOIN athlete_group 		as ag 	ON (a.athlete_id = ag.athlete_id)
                                JOIN groups 			as g 	ON (ag.group_id = g.group_id)
                                JOIN group_session 		as gs	ON (g.group_id = gs.group_id)
                                JOIN group_discipline	as gd	ON (g.group_id = gd.group_id)
                            WHERE g.group_number = '$group' AND gd.discipline_id = '$discipline' AND gs.session_id = '$session'";
                            
            if($score_result = $db->query($score_query)){
                $return_array = array();
                while($score_row = mysqli_fetch_array($score_result)){
                    $s_info = array();
                    $s_info['athlete_id'] = $score_row['athlete_id'];
                    $s_info['score'] = $score_row['final_score'];
                    array_push($return_array, $s_info);
                }
                $data['scores'] = $return_array;
            }
        }
    }    
}

echo json_encode($data);
?>