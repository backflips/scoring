<?php

require_once('db_connect.php');
$db = connect_to_db();

$data = array();
$errors = array();

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    if(empty($_POST['athlete_id'])) {
        $errors['athlete'] = "Error retrieveing athlete id.";
    }
    if(empty($_POST['event_id'])) {
        $errors['event'] = "Error retrieveing event id.";
    }
    if(empty($_POST['session_id'])) {
        $errors['session'] = "Error retrieveing session id.";
    }
    if(empty($_POST['d_score'])) {
        $errors['d_score'] = "A D-Score is required";
    }
    if(empty($_POST['e_score'])) {
        $errors['e_score'] = "A E-Score is required";
    }
    if(empty($_POST['final_score'])) {
        $_errors['final_score'] = "A Final Score is required";
    }
    if( ! empty($errors)) {
        $data['success'] = false;
        $data['errors'] = $errors;
    } else {
        $athlete_id = validate($db, $_POST['athlete_id']);
        $event_id = validate($db, $_POST['event_id']);
        $session_id = validate($db, $_POST['session_id']);
        $d_score = validate($db, $_POST['d_score']);
        $e_score = validate($db, $_POST['e_score']);
        $e1_score = validate($db, $_POST['e1_score']);
        $e2_score = validate($db, $_POST['e2_score']);
        $nd_score = validate($db, $_POST['nd_score']);
        $final_score = validate($db, $_POST['final_score']);
        
        if(empty($_POST['score_id'])) {
            $query = "CALL insert_score('$athlete_id','$event_id','$session_id','$d_score','$e_score','$e1_score','$e2_score','$nd_score','$final_score')";
        }
        else{
            $score_id = validate($db, $_POST['score_id']);
            $query = "CALL update_score('$score_id', '$d_score','$e_score','$e1_score','$e2_score','$nd_score','$final_score')";
        }
        
        if($result = $db->query($query)) {
            $data['success'] = true;
        }
        else{
            $data['success'] = false;
            $errors['mysql'] = 'Unable to save.  There was an error with the database! ' . $db->error;
            $data['errors'] = $errors;
        }
    }
}
echo json_encode($data);

