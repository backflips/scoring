DROP TABLE IF EXISTS athlete_club;
DROP TABLE IF EXISTS athlete_category;
DROP TABLE IF EXISTS athlete_session;
DROP TABLE IF EXISTS athlete_score;
DROP TABLE IF EXISTS athlete_group;
DROP TABLE IF EXISTS athlete_discipline;
DROP TABLE IF EXISTS category_discipline;
DROP TABLE IF EXISTS category_session;
DROP TABLE IF EXISTS event_discipline;
DROP TABLE IF EXISTS event_order;
DROP TABLE IF EXISTS discipline_session;
DROP TABLE IF EXISTS group_session;
DROP TABLE IF EXISTS group_discipline;
DROP TABLE IF EXISTS group_event_start;
DROP TABLE IF EXISTS athlete_group_order;
DROP TABLE IF EXISTS athlete;
DROP TABLE IF EXISTS club;
DROP TABLE IF EXISTS event;
DROP TABLE IF EXISTS score;
DROP TABLE IF EXISTS discipline;
DROP TABLE IF EXISTS category;
DROP TABLE IF EXISTS groups;
DROP TABLE IF EXISTS sessions;
DROP PROCEDURE IF EXISTS insert_athlete;
DROP PROCEDURE IF EXISTS insert_cat;
DROP PROCEDURE IF EXISTS insert_event;
DROP PROCEDURE IF EXISTS add_athlete_group;
DROP PROCEDURE IF EXISTS insert_score;
DROP PROCEDURE IF EXISTS update_score;
DROP PROCEDURE IF EXISTS remove_athlete_group;
DROP PROCEDURE IF EXISTS move_athlete_order_up;
DROP PROCEDURE IF EXISTS move_athlete_order_down;
DROP VIEW IF EXISTS view_athlete;
DROP TRIGGER IF EXISTS del_club;
DROP TRIGGER IF EXISTS del_cat;



CREATE TABLE athlete (
    athlete_id INT PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    age INT NOT NULL  
);

CREATE TABLE club (
    club_id INT PRIMARY KEY AUTO_INCREMENT,
    club_name VARCHAR(255) NOT NULL,
    club_location VARCHAR(255) NOT NULL
);

CREATE TABLE event (
    event_id INT PRIMARY KEY AUTO_INCREMENT,
    event_name VARCHAR(255) NOT NULL
);

CREATE TABLE score (
    score_id INT PRIMARY KEY AUTO_INCREMENT,
    d_score FLOAT,
    e1_score FLOAT,
    e2_score FLOAT,
    e3_score FLOAT,
    e4_score FLOAT,
    eave_score FLOAT,
    ef_score FLOAT,
    nd_score FLOAT,
    final_score FLOAT
);

CREATE TABLE discipline (
    discipline_id INT PRIMARY KEY AUTO_INCREMENT,
    discipline_name VARCHAR(255) NOT NULL
);

CREATE TABLE category (
    category_id INT PRIMARY KEY AUTO_INCREMENT,
    category_name VARCHAR(255) NOT NULL
);

CREATE TABLE groups (
    group_id INT PRIMARY KEY AUTO_INCREMENT,
    group_number INT NOT NULL
);

CREATE TABLE sessions (
    session_id INT PRIMARY KEY AUTO_INCREMENT,
    session_number INT NOT NULL
);

CREATE TABLE athlete_club (
    athlete_club_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    club_id INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (club_id) REFERENCES club(club_id) ON DELETE CASCADE
);

CREATE TABLE athlete_category (
    athlete_category_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    category_id INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (category_id) REFERENCES category(category_id) ON DELETE CASCADE
);

CREATE TABLE athlete_group (
    athlete_group_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    group_id INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE
);

CREATE TABLE athlete_session (
    athlete_session_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    session_id INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (session_id) REFERENCES sessions(session_id) ON DELETE CASCADE
);

CREATE TABLE discipline_session (
        discipline_session_id INT PRIMARY KEY AUTO_INCREMENT,
        discipline_id INT NOT NULL,
        session_id INT NOT NULL,
        FOREIGN KEY (discipline_id) REFERENCES discipline(discipline_id) ON DELETE CASCADE,
        FOREIGN KEY (session_id) REFERENCES sessions(session_id) ON DELETE CASCADE
);

CREATE TABLE athlete_score (
    athlete_score_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    score_id INT NOT NULL,
    event_id INT NOT NULL,
    session_id INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (score_id) REFERENCES score(score_id) ON DELETE CASCADE,
    FOREIGN KEY (event_id) REFERENCES event(event_id) ON DELETE CASCADE,
    FOREIGN KEY (session_id) REFERENCES sessions(session_id) ON DELETE CASCADE
);

CREATE TABLE athlete_discipline (
        athlete_discipline_id INT PRIMARY KEY AUTO_INCREMENT,
        athlete_id INT NOT NULL,
        discipline_id INT NOT NULL,
        FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
        FOREIGN KEY (discipline_id) REFERENCES discipline(discipline_id)
);

CREATE TABLE event_discipline (
        event_discipline_id INT PRIMARY KEY AUTO_INCREMENT,
        event_id INT NOT NULL,
        discipline_ID INT NOT NULL,
        FOREIGN KEY (event_id) REFERENCES event(event_id) ON DELETE CASCADE,
        FOREIGN KEY (discipline_id) REFERENCES discipline(discipline_id)
);

CREATE TABLE event_order (
        event_order_id INT PRIMARY KEY AUTO_INCREMENT,
        event_id INT NOT NULL,
        event_order INT NOT NULL,
        FOREIGN KEY (event_id) REFERENCES event(event_id) ON DELETE CASCADE
);

CREATE TABLE category_discipline (
    category_discipline_id INT PRIMARY KEY AUTO_INCREMENT,
    category_id INT NOT NULL,
    discipline_id INT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(category_id) ON DELETE CASCADE,
    FOREIGN KEY (discipline_id) REFERENCES discipline(discipline_id) ON DELETE CASCADE
);

CREATE TABLE category_session (
    category_session_id INT PRIMARY KEY AUTO_INCREMENT,
    category_id INT NOT NULL,
    session_id INT NOT NULL,
    FOREIGN KEY (category_id) REFERENCES category(category_id) ON DELETE CASCADE,
    FOREIGN KEY (session_id) REFERENCES sessions(session_id) ON DELETE CASCADE
);

CREATE TABLE group_session (
    group_session_id INT PRIMARY KEY AUTO_INCREMENT,
    group_id INT NOT NULL,
    session_id INT NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE,
    FOREIGN KEY (session_id) REFERENCES sessions(session_id) ON DELETE CASCADE
);

CREATE TABLE group_event_start (
    group_event_start_id INT PRIMARY KEY AUTO_INCREMENT,
    group_id INT NOT NULL,
    event_id INT NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE,
    FOREIGN KEY (event_id) REFERENCES event(event_id) ON DELETE CASCADE
);

CREATE TABLE group_discipline (
    group_discipline_id INT PRIMARY KEY AUTO_INCREMENT,
    group_id INT NOT NULL,
    discipline_id INT NOT NULL,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE,
    FOREIGN KEY (discipline_id) REFERENCES discipline(discipline_id) ON DELETE CASCADE
);

CREATE TABLE athlete_group_order (
    athlete_group_order_id INT PRIMARY KEY AUTO_INCREMENT,
    athlete_id INT NOT NULL,
    group_id INT NOT NULL,
    order_number INT NOT NULL,
    FOREIGN KEY (athlete_id) REFERENCES athlete(athlete_id) ON DELETE CASCADE,
    FOREIGN KEY (group_id) REFERENCES groups(group_id) ON DELETE CASCADE
);


delimiter //
CREATE PROCEDURE insert_athlete(IN fname VARCHAR(255), IN lname VARCHAR(255), IN a INT, IN c_id INT, IN cat_id INT, IN d_id INT)
BEGIN
        DECLARE a_id INT DEFAULT 0;
        INSERT INTO athlete(first_name, last_name, age) VALUES(fname, lname, a);
        SET a_id = LAST_INSERT_ID();
        INSERT INTO athlete_club(athlete_id, club_id) VALUES(a_id, c_id);
        INSERT INTO athlete_discipline(athlete_id, discipline_id) VALUES(a_id,d_id);
        INSERT INTO athlete_category(athlete_id, category_id) VALUES(a_id, cat_id);
END //

CREATE PROCEDURE insert_cat(IN catname VARCHAR(255), IN d_id INT)
BEGIN
        DECLARE cat_id INT DEFAULT 0;
        INSERT INTO category(category_name) VALUES(catname);
        SET cat_id = LAST_INSERT_ID();
        INSERT INTO category_discipline(category_id, discipline_id) VALUES(cat_id, d_id);
END //

CREATE PROCEDURE insert_event(IN ename VARCHAR(255), IN d_id INT, IN o_num INT)
BEGIN
        DECLARE e_id INT DEFAULT 0;
        INSERT INTO event(event_name) VALUES(ename);
        SET e_id = LAST_INSERT_ID();
        INSERT INTO event_discipline(event_id, discipline_id) VALUES(e_id, d_id);
        INSERT INTO event_order(event_id, event_order) VALUES(e_id, o_num);
END //

CREATE PROCEDURE add_athlete_group(IN a_id INT, IN g_id INT)
BEGIN
        SELECT session_id FROM group_session WHERE group_id = g_id LIMIT 1 INTO @s_id;
        INSERT INTO athlete_group(athlete_id, group_id) VALUES(a_id, g_id);
        INSERT INTO athlete_session(athlete_id, session_id) VALUES(a_id, @s_id);
        INSERT INTO athlete_group_order(athlete_id, group_id, order_number) VALUES (a_id, g_id, (SELECT COUNT(a.athlete_id) FROM athlete AS a JOIN athlete_group AS ag ON(a.athlete_id = ag.athlete_id) WHERE ag.group_id = g_id));
END //

CREATE PROCEDURE remove_athlete_group(IN a_id INT, IN g_id INT)
BEGIN     
        DECLARE num INT DEFAULT 0;
        SELECT order_number INTO num FROM athlete_group_order WHERE athlete_id = a_id AND group_id = g_id;
        UPDATE athlete_group_order SET order_number = order_number-1 WHERE group_id = g_id AND order_number > num;
        DELETE FROM athlete_group_order WHERE athlete_id = a_id AND group_id = g_id;
        DELETE FROM athlete_group WHERE athlete_id = a_id AND group_id = g_id;
END //

CREATE PROCEDURE move_athlete_order_up(IN a_id INT, IN g_id INT)
BEGIN
        DECLARE num INT DEFAULT 0;
        SELECT order_number INTO num FROM athlete_group_order WHERE athlete_id = a_id AND group_id = g_id;
        UPDATE athlete_group_order SET order_number = order_number + 1 WHERE order_number = num - 1 AND group_id = g_id;
        UPDATE athlete_group_order SET order_number = order_number - 1 WHERE athlete_id = a_id AND group_id = g_id AND order_number > 1;
END //

CREATE PROCEDURE move_athlete_order_down(IN a_id INT, IN g_id INT)
BEGIN
        DECLARE num INT DEFAULT 0;
        DECLARE max INT DEFAULT 0;
        SELECT COUNT(athlete_id) INTO max FROM athlete_group_order WHERE group_id = g_id;
        SELECT order_number INTO num FROM athlete_group_order WHERE athlete_id = a_id AND group_id = g_id;
        UPDATE athlete_group_order SET order_number = order_number - 1 WHERE order_number = num + 1 AND group_id = g_id;
        UPDATE athlete_group_order SET order_number = order_number + 1 WHERE athlete_id = a_id AND group_id = g_id AND order_number < max;
END //

CREATE PROCEDURE insert_score(IN a_id INT, IN e_id INT, IN s_id INT, IN dscore FLOAT, IN escore FLOAT, IN e1score FLOAT, IN e2score FLOAT, IN ndscore FLOAT, IN finalscore FLOAT)
BEGIN
        DECLARE scoreid INT DEFAULT 0;
        INSERT INTO score(d_score, e1_score, e2_score, ef_score, nd_score, final_score) VALUES(dscore, e1score, e2score, escore, ndscore, finalscore);
        SET scoreid = LAST_INSERT_ID();
        INSERT INTO athlete_score(athlete_id, score_id, event_id, session_id) VALUES(a_id, scoreid, e_id, s_id);
END //

CREATE PROCEDURE update_score(IN s_id INT, IN dscore FLOAT, IN escore FLOAT, IN e1score FLOAT, IN e2score FLOAT, IN ndscore FLOAT, IN finalscore FLOAT)
    UPDATE score
    SET d_score=dscore, ef_score=escore, e1_score=e1score, e2_score=e2score, nd_score=ndscore, final_score=finalscore
    WHERE score_id = s_id;//

CREATE TRIGGER del_club BEFORE DELETE ON club
FOR EACH ROW
BEGIN
DELETE a FROM athlete AS a JOIN athlete_club as ac ON a.athlete_id = ac.athlete_id
WHERE ac.club_id = OLD.club_id;
END;//

CREATE TRIGGER del_cat BEFORE DELETE ON category
FOR EACH ROW
BEGIN
DELETE a FROM athlete AS a JOIN athlete_category as ac ON a.athlete_id = ac.athlete_id
WHERE ac.category_id = OLD.category_id;
END;//      

delimiter ;

CREATE VIEW view_athlete AS SELECT a.athlete_id, a.first_name, a.last_name, a.age, c.club_name, cat.category_name
                        FROM athlete a
                        INNER JOIN athlete_club ac ON ( ac.athlete_id = a.athlete_id ) 
                        INNER JOIN club c ON ( ac.club_id = c.club_id )
                        INNER JOIN athlete_category acat ON (a.athlete_id = acat.athlete_id)
                        INNER JOIN category cat ON (acat.category_id = cat.category_id)
                        ORDER BY c.club_name, a.last_name, a.first_name;
                        

-- ---------SAMPLE DATA--------------
                        
INSERT INTO discipline(discipline_name) VALUES('MAG'), ('WAG');
CALL insert_event('Floor', 1, 1);
CALL insert_event('Pommel Horse', 1, 2);
CALL insert_event('Rings', 1, 3);
CALL insert_event('Vault', 1, 4);
CALL insert_event('Parallel Bars', 1, 5);
CALL insert_event('Horizontal Bars', 1, 6);
CALL insert_event('Vault', 2, 1);
CALL insert_event('Balance Beam', 2, 3);
CALL insert_event('Uneven Bars', 2, 2);
CALL insert_event('Floor', 2, 4);

CALL insert_cat('Senior', 1);
CALL insert_cat('Junior', 1);
CALL insert_cat('Level 2', 2);
CALL insert_cat('Level 1', 2);

INSERT INTO sessions(session_number) VALUES (1), (2), (3);
INSERT INTO category_session(category_id, session_id) VALUES (1,1), (2,2);
INSERT INTO club(club_name, club_location) VALUES('Delta Gymnastics', 'Delta'), ('Surrey Gymnastics', 'Surrey');
CALL insert_athlete('test1', 'test1', 24, 1, 1, 1);
CALL insert_athlete('test2', 'test2', 24, 1, 1, 1);
CALL insert_athlete('test3', 'test3', 24, 1, 1, 1);
CALL insert_athlete('test4', 'test4', 24, 2, 1, 1);
CALL insert_athlete('test5', 'test5', 24, 2, 1, 1);
CALL insert_athlete('test6', 'test6', 24, 2, 1, 1);


INSERT INTO discipline_session(discipline_id, session_id) VALUES (1,2);
INSERT INTO discipline_session(discipline_id, session_id) VALUES (2,2);
INSERT INTO discipline_session(discipline_id, session_id) VALUES (1,1);
INSERT INTO discipline_session(discipline_id, session_id) VALUES (2,1);
INSERT INTO discipline_session(discipline_id, session_id) VALUES (1,3);
INSERT INTO discipline_session(discipline_id, session_id) VALUES (2,3);

INSERT INTO groups(group_number) VALUES (1), (2), (3), (4), (5), (6), (7);
INSERT INTO group_session(group_id,session_id) VALUES (1,2), (2,2), (3,2), (4,2), (5,2), (6,2);
INSERT INTO group_session(group_id,session_id) VALUES (7,1);
INSERT INTO group_discipline(group_id, discipline_id) VALUES (1,1), (2,1), (3,1), (4,1), (5,1), (6,1), (7,2);
INSERT INTO group_event_start(group_id, event_id) VALUES (1, 1), (2,2), (3,3), (4,4), (5,5), (6,6);


CALL add_athlete_group(1,1);
CALL add_athlete_group(2,1);
CALL add_athlete_group(3,2);
CALL add_athlete_group(4,1);
CALL add_athlete_group(5,4);
CALL add_athlete_group(6,3);

CALL insert_athlete('tester1', 'test1', 24, 1, 1, 1);
CALL insert_athlete('tester2', 'test2', 24, 1, 1, 1);
CALL insert_athlete('tester3', 'test3', 24, 1, 1, 1);
CALL insert_athlete('tester4', 'test4', 24, 2, 1, 1);
CALL insert_athlete('tester5', 'test5', 24, 2, 1, 1);
CALL insert_athlete('tester6', 'test6', 24, 2, 1, 1);

CALL add_athlete_group(7,1);
CALL add_athlete_group(8,1);
CALL add_athlete_group(9,4);
CALL add_athlete_group(10,3);
CALL add_athlete_group(11,3);
CALL add_athlete_group(12,3);