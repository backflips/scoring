<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>View Athletes</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
    
    </head>
    <body>
        <?php require_once("navbar.php");
            error_reporting(E_ALL);
            
            require_once('db_connect.php');                      
            $db = connect_to_db();
            $query = "SELECT * FROM view_athlete";
                        
            if($result = $db->query($query)){
        ?>
        
        <div class="container">
            <table class="table table-hover table-striped" id="athlete-table">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Club</th>
                        <th>Category</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            while($row = mysqli_fetch_array($result)) {
                    ?>
                    
                    <tr id="athlete<?php echo $row['athlete_id'];?>">
                        <td class="col-xs-3"><?php echo $row['first_name'] . " " . $row['last_name']; ?></td>
                        <td class="col-xs-1"><?php echo $row['age']; ?></td>
                        <td class="col-xs-3"><?php echo $row['club_name']; ?></td>
                        <td class="col-xs-2"><?php echo $row['category_name']; ?></td>
                        <td class="col-xs-1"><span class="glyphicon glyphicon-pencil text-primary"></span> <a class="delete-link" id="<?php echo $row['athlete_id'];?>" href="./"><span class="glyphicon glyphicon-remove text-danger"></span></a></td>
                    </tr>
                    <?php }
                    
            }
            else {
                echo $db->errno . $db->error;
            }?>
                    
                </tbody>
            </table>
        </div>
        
        <?php require_once("footer.php"); ?>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        
        <script src="js/deleteathlete.js"></script>         
        
    </body>
</html>