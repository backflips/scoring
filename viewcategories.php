<!DOCTYPE html>
<html lang="en">
    <head> 
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <title>View Athletes</title>
        
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/navbar-footer.css" rel="stylesheet">
    
    </head>
    <body>
        <?php require_once("navbar.php");
            error_reporting(E_ALL);
            
            require_once('db_connect.php');                      
            $db = connect_to_db();
            $query =    "SELECT c.category_id, c.category_name, count(a.athlete_id) as total
                         FROM category c LEFT JOIN athlete_category a ON (a.category_id = c.category_id)
                         GROUP BY c.category_name
                         ORDER BY total DESC";
                        
            if($result = $db->query($query)){
        ?>
        
        <div class="container">
            <table class="table table-hover table-striped" id="category-table">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th># of Athletes</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                            while($row = mysqli_fetch_array($result)) {
                    ?>
                    
                    <tr id="category<?php echo $row['category_id'];?>">
                        <td><?php echo $row['category_name']; ?></td>
                        <td><?php echo $row['total']; ?></td>
                        <td class="col-xs-1"><span class="glyphicon glyphicon-pencil text-primary"></span> <a class="delete-link" id="<?php echo $row['category_id'];?>" href="./"><span class="glyphicon glyphicon-remove text-danger"></span></a></td>
                    </tr>
                    <?php }} ?>
                    
                </tbody>
            </table>
        </div>
        
        <?php require_once("footer.php"); ?>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.11.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/deletecat.js"></script>
    </body>
</html>